package com.Kordata.cursojava.controllers;

import com.Kordata.cursojava.dao.ArticuloDao;
import com.Kordata.cursojava.models.Articulo;
import com.Kordata.cursojava.models.VentasDetalle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ArticuloController {


    @Autowired
    private ArticuloDao articuloDao;

    @RequestMapping(value = "api/articulos", method = RequestMethod.GET)
    public List<Articulo> getArticulos(){
        System.out.println("quevergue...");
        return articuloDao.getArticulos();
    }

    @RequestMapping(value = "api/articulos/{id}", method = RequestMethod.DELETE)
    public void eliminar(@PathVariable long id){
        articuloDao.eliminar(id);
    }

    @RequestMapping(value = "api/articulos", method = RequestMethod.POST)
    public void registrarArticulo(@RequestBody Articulo articulo){
        articuloDao.registrar(articulo);
    }

    @RequestMapping(value = "api/articulo/{parte}", method = RequestMethod.GET)
    public List<Articulo> getArticulos(@PathVariable("parte") String parte){

        System.out.println("entrando a getArticulos()...");
        System.out.println("parte: " + parte);

        List<Articulo> articulos = articuloDao.getArticulos();
        List articulosResultado = new ArrayList();

        for(int i = 0; i<articulos.size(); i++) {
            System.out.println("articulos.get(i).getDescripcion():" + articulos.get(i).getDescripcion());
            if (articulos.get(i).getDescripcion().compareTo(parte) == 0){
                System.out.println(articulos.get(i).getDescripcion() + "==" + parte);
            }
        }

        System.out.println("fin de un cilo inicio de otro");

        for(int i = 0; i<articulos.size(); i++) {
            System.out.println("articulos.get(i).getDescripcion():" + articulos.get(i).getDescripcion());
            if (articulos.get(i).getDescripcion().contains(parte)){

                System.out.println(articulos.get(i).getDescripcion() + " contains " + parte);
                articulosResultado.add(articulos.get(i));
            }
        }

        System.out.println("articulos.size(): " + articulos.size());
        System.out.println("articulosResultado.size(): " + articulosResultado.size());

        System.out.println("saliendo...");

        return articulosResultado;
    }
}
