package com.Kordata.cursojava.controllers;

import com.Kordata.cursojava.models.VentasDetalle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@RestController
public class VentaDetalleController {
    @Autowired
    private com.Kordata.cursojava.dao.VentaDetalleDao ventaDetalleDao;

    @RequestMapping(value = "api/ventasdetalle", method = RequestMethod.GET)
    public List<VentasDetalle> getVentasDetalle(){
        return ventaDetalleDao.getVentasDetalle();
    }

    @RequestMapping(value = "api/ventasdetalle", method = RequestMethod.POST)
    public void registrarVentaDetalle(@RequestBody VentasDetalle ventaDetalle){

        if (ventaDetalle.getVentasDetallePK() == null)
            System.out.println("ventaDetalle.getVentasDetallePK() es bien pinche nulo.");

        ventaDetalleDao.registrar(ventaDetalle);
    }



}
