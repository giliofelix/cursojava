package com.Kordata.cursojava.controllers;

import com.Kordata.cursojava.dao.VentaDao;
import com.Kordata.cursojava.models.Venta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class VentaController {
    @Autowired
    private com.Kordata.cursojava.dao.VentaDao ventaDao;

    @RequestMapping(value = "api/ventas", method = RequestMethod.GET)
    public List<Venta> getVentas(){
        return ventaDao.getVentas();
    }

    @RequestMapping(value = "api/ventas/{id}", method = RequestMethod.DELETE)
    public void eliminar(@PathVariable long id){
        ventaDao.eliminar(id);
    }

    @RequestMapping(value = "api/ventas", method = RequestMethod.POST)
    public void registrarVenta(@RequestBody Venta venta){
        ventaDao.registrar(venta);
    }
}
