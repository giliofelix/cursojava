package com.Kordata.cursojava.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "ventas")
@ToString
@EqualsAndHashCode
public class Venta {
    @Id
    @Getter
    @Setter
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Getter @Setter @Column(name = "Fecha")
    private Date fecha;

    @Transient
    private List<VentasDetalle> ventasDetalle;
}
