package com.Kordata.cursojava.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Embeddable
public class VentasDetallePK implements Serializable {

    @Getter
    @Setter
    @Column(name = "id_venta")
    private Integer idVenta;

    @Getter
    @Setter
    @Column(name = "id_articulo")
    private Integer idArticulo;
}
