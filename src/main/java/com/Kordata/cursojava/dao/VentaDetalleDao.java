package com.Kordata.cursojava.dao;

import com.Kordata.cursojava.models.VentasDetalle;

import java.util.List;

public interface VentaDetalleDao {

    List<VentasDetalle> getVentasDetalle();

    void registrar(VentasDetalle ventaDetalle);
}
