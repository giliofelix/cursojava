package com.Kordata.cursojava.dao;

import com.Kordata.cursojava.models.Venta;

import java.util.List;

public interface VentaDao {

    List<Venta> getVentas();

    void eliminar(long Id);

    void registrar(Venta venta);
}
