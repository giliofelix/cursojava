package com.Kordata.cursojava.dao;

import com.Kordata.cursojava.models.Articulo;

import java.util.List;

public interface ArticuloDao {
    List<Articulo> getArticulos();

    void eliminar(long id);

    void registrar(Articulo articulo);
}
