package com.Kordata.cursojava.dao;

import com.Kordata.cursojava.models.Venta;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class VentaDaoImp implements VentaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Venta> getVentas() {
        return entityManager.createQuery("FROM Venta").getResultList();
    }

    @Override
    public void eliminar(long Id) {
        Venta venta = entityManager.find(Venta.class, Id);
        entityManager.remove(venta);
    }

    @Override
    public void registrar(Venta venta) {
        entityManager.merge(venta);
    }
}
