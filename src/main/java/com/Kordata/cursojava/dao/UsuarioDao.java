package com.Kordata.cursojava.dao;

import com.Kordata.cursojava.models.Usuario;

import java.util.List;

public interface UsuarioDao {
    List<Usuario> getUsuarios();

    void eliminar(long id);

    void registrar(Usuario usuario);
}
