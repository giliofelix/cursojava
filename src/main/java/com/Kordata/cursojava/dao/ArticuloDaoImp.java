package com.Kordata.cursojava.dao;

import com.Kordata.cursojava.models.Articulo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class ArticuloDaoImp implements ArticuloDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Articulo> getArticulos() {
        return entityManager.createQuery("FROM Articulo").getResultList();
    }

    @Override
    public void eliminar(long id) {
        Articulo articulo = entityManager.find(Articulo.class, id);
        entityManager.remove(articulo);
    }

    @Override
    public void registrar(Articulo articulo) {

        entityManager.merge(articulo);
    }
}
