package com.Kordata.cursojava.dao;

import com.Kordata.cursojava.models.Venta;
import com.Kordata.cursojava.models.VentasDetalle;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class VentaDetalleDaoImp implements VentaDetalleDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<VentasDetalle> getVentasDetalle() {
        return entityManager.createQuery("FROM VentasDetalle").getResultList();
    }

    @Override
    public void registrar(VentasDetalle ventaDetalle) {

        System.out.println("id venta: " + ventaDetalle.getVentasDetallePK().getIdVenta());
        System.out.println("id art: " + ventaDetalle.getVentasDetallePK().getIdArticulo());

        entityManager.merge(ventaDetalle);
    }
}
