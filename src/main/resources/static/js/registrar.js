$(document).ready(function() {
    // onready
});


async function registrarUsuario(){

    let datos = {};

    datos.nombre = document.querySelector('#txtNombre').value;
    datos.apellido = document.querySelector('#txtApellido').value;
    datos.email = document.querySelector('#txtEmail').value;
    datos.password = document.querySelector('#txtPassword').value;

    let repetirPassword = document.querySelector('#txtRepetirPassword').value;

    if (repetirPassword != datos.password )
    {
        alert('La confirmación de la contraseña no coincide');
        return;
    }

    const request = await fetch('api/usuarios', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(datos)
        });

    const usuarios = await request.json();

}
